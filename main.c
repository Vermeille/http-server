/*
 ** main.c for C_network
 **
 ** Made by Guillaume "Vermeille" Sanchez
 ** Login   sanche_g <Guillaume.V.Sanchez@gmail.com>
 **
 ** Started on  ven. 18 nov. 2011 14:30:24 CET Guillaume "Vermeille" Sanchez
 ** Last update sam. 19 nov. 2011 21:00:52 CET Guillaume "Vermeille" Sanchez
 */
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>

#include "network.h"

void* handle_connection(void* data)
{
    static int nbthread = 0;
    printf("nbthread = %d\n", ++nbthread);
    printf("Dans le thread !\n");
    fflush(stdout);
    int csock = *(int*)data;
    char* msg = get_http_request(csock);

    char* path = get_http_requested_file(msg);
    free(msg);
    if (path[0] == '\0')
        send_file(csock, "index.html");
    else {
        if (is_authorized_path(path))
            send_file(csock, path);
        else
            send_file(csock, "403.html");
    }
    free(path);
    close(csock);
    printf("nbthread = %d\n", --nbthread);
    return NULL;
}

int main(int argc, char **argv)
{
    int sock = open_tcp_server(80);

    while (1) {
        pthread_t thread;
        int csock = wait_for_client(sock);
        pthread_create(&thread, NULL, handle_connection, (void*)&csock);
    }
    close(sock);



    return 0;
}

