/*
 ** network.c for C_network
 **
 ** Made by Guillaume "Vermeille" Sanchez
 ** Login   sanche_g <Guillaume.V.Sanchez@gmail.com>
 **
 ** Started on  sam. 19 nov. 2011 02:39:28 CET Guillaume "Vermeille" Sanchez
 ** Last update ven. 25 nov. 2011 09:10:40 CET Guillaume "Vermeille" Sanchez
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>

#include "network.h"

int tcp_socket(int port, struct sockaddr_in* sin)
{
    int sock = socket(AF_INET, SOCK_STREAM, 0);
    sin->sin_addr.s_addr = htonl(INADDR_ANY);
    sin->sin_family = AF_INET;
    sin->sin_port = htons(port);
    return sock;
}

int open_tcp_server(int port)
{
    struct sockaddr_in sin;
    int sock = tcp_socket(80, &sin);

    if (bind(sock, (struct sockaddr*)&sin, sizeof(sin)) == -1) {
        printf("Erreur d'ouverture de la socket\n");
        exit(1);
        return EXIT_FAILURE;
    }

    if (listen(sock, 5) == -1) {
        printf("Ne peut pas écouter sur le socket\n");
        exit(1);
        return EXIT_FAILURE;
    }

    return sock;
}

int wait_for_client(int socket)
{
    struct sockaddr_in csin;
    int csock;
    unsigned int csize = sizeof(csin);
    if ((csock = accept(socket, (struct sockaddr*)&csin, &csize)) == -1) {
        printf("Ne peut pas accepter de connexion sur la socket\n");
        exit(1);
        return EXIT_FAILURE;
    }

    return csock;
}

char* get_http_request(int csocket)
{
    char buffer[27];
    char* msg = NULL;

    int size = 0, totalsize = 0;

    do {
        while ((size = recv(csocket, buffer, 26, 0)) == -1)
            ;
        buffer[size] = '\0';
        totalsize += size;
        msg = (char*) realloc(msg, totalsize+1);
        strcpy(msg + totalsize - size, buffer);
    } while (totalsize < 10000 && strncmp(&msg[totalsize-4], "\r\n\r\n", 4));
    return msg;
}

char* get_http_requested_file(char* http_request)
{
    int size = 0;
    char *path = (char*) realloc(NULL, 10);
    if (!strncmp(http_request, "GET", 3)) {
        int i = 0;
        for (i = 0 ; http_request[i+5] != ' ' ; ++i) {
            if (i % 10 == 0) {
                path = (char*) realloc(path, i+10);
            }
            path[i] = http_request[i+5];
        }
        path[i] = '\0';
    }
    return path;
}

void send_file(int csocket, char* path)
{
    enum FileType t = get_file_type(path);
    FILE* file;
    if (access(path, F_OK) != 0) {
        send_file(csocket, "404.html");
        return;
    }
    if (t == script) {
        char command[strlen(path)+2];
        sprintf(command, "./%s", path);
        file = popen(command, "r");
    } else if (t == php) {
        char command[strlen(path)+4];
        sprintf(command, "php %s", path);
        file = popen(command, "r");
    }

    else {
        while ((file = fopen(path, "r")) == NULL)
            ;
    }
    int size = 0;
    char html[513];
    do {
        size = fread(html, 1, 512, file);
        html[size] = '\0';
        send(csocket, html, size, 0);
    } while (size >=10);
    
    if (t == script)
        pclose(file);
    else
        fclose(file);

}

int is_authorized_path(char* path)
{
    return (path[0] != '/') && (path[0] != '.' && path[1] != '.');
}

enum FileType get_file_type(char* file)
{
    int i = strlen(file) - 1;
    while (i >= 0 && file[i] != '.')
        --i;
    printf("%s\n", file + i+1);
    if (!strcmp(file + i, ".html"))
        return text;
    if (!strcmp(file + i, ".sh") || !strcmp(file+i, ".py"))
        return script;
    if (!strcmp(file + i, ".php"))
        return php;
    return -1;
}

