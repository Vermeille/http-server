enum FileType {
    text,
    php,
    script
};

int tcp_socket(int port, struct sockaddr_in* sin);
int open_tcp_server(int port);
int wait_for_client(int socket);
char* get_http_request(int csocket);
char* get_http_requested_file(char* http_request);
void send_file(int csocket, char* path);
int is_authorized_path(char* path);
enum FileType get_file_type(char* path); 
